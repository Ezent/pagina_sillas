<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

Route::get('/', function () {
    return view('plantilla.welcome');
});
Route::get('/mobiliario', function() {
    $mobiliario= array(
        ['nombre' => 'Sillas', 'titulo' => 'Silla Acojinada', 'img' => 'img/mobiliario/sillas/silla_acojinada.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'Sillas', 'titulo' => 'Silla Tiffani', 'img' => 'img/mobiliario/sillas/silla_tiffani.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'Mesas', 'titulo' => 'Mesa Rectangular', 'img' => 'img/mobiliario/mesas/tablon_recto.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'Mesas', 'titulo' => 'Mesa Redonda', 'img' => 'img/mobiliario/mesas/tablon_redondo.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'Manteleria', 'titulo' => 'Manteles para mesa rectangular', 'img' => 'img/mobiliario/Manteleria/m_recta.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'Manteleria', 'titulo' => 'Manteles para mesa redonda', 'img' => 'img/mobiliario/Manteleria/m_redonda.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'Manteleria', 'titulo' => 'Listones y Fundas', 'img' => 'img/mobiliario/Manteleria/listones_y_funda.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ']
    );
    return view('contenido.mobiliario', ['mobiliario' => $mobiliario]);
});

Route::get('/galeria', function() {
    $galeria = array(
        ['nombre' => 'Combinacion', 'titulo' => 'Combinaciones de mesa', 'img' => 'img/galeria/azul_cielo.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'Combinacion', 'titulo' => 'Combinaciones de mesa', 'img' => 'img/galeria/azul_rey.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'Combinacion', 'titulo' => 'Combinaciones de mesa', 'img' => 'img/galeria/rosa_pastel.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'Combinacion', 'titulo' => 'Combinaciones de mesa', 'img' => 'img/galeria/rojo_bandera.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'Combinacion', 'titulo' => 'Combinaciones de mesa', 'img' => 'img/galeria/vino.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'Combinacion_sillas', 'titulo' => 'Combinaciones de silla', 'img' => 'img/galeria/tiffany.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'Combinacion_sillas', 'titulo' => 'Combinaciones de silla', 'img' => 'img/galeria/plegable_acojinada.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'Combinacion_sillas', 'titulo' => 'Combinaciones de silla', 'img' => 'img/galeria/plegables_funda_y_lazo.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'arreglos_&_decoraciones', 'titulo' => 'Arreglo y decoraciones', 'img' => 'img/galeria/m_r_silla_funda_y_lazo.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'arreglos_&_decoraciones', 'titulo' => 'Arreglo y decoraciones', 'img' => 'img/galeria/m_r_sillas_plegables.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'arreglos_&_decoraciones', 'titulo' => 'Arreglo y decoraciones', 'img' => 'img/galeria/m_r_tiffany_lazo.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'arreglos_&_decoraciones', 'titulo' => 'Arreglo y decoraciones', 'img' => 'img/galeria/m_rect_sillas_tiffany.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'arreglos_&_decoraciones', 'titulo' => 'Arreglo y decoraciones', 'img' => 'img/galeria/m_rect_tiffany_sin_mantel.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'arreglos_&_decoraciones', 'titulo' => 'LArreglo y decoraciones', 'img' => 'img/galeria/m_rect_sillas_plegables.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '],
        ['nombre' => 'arreglos_&_decoraciones', 'titulo' => 'Arreglo y decoraciones', 'img' => 'img/galeria/m_rect_plegable_funda_y_lazo.png', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ']
    );
    return view('contenido.galeria', ['galeria' => $galeria]);
});

Route::get('/eventos', function() {
    return view('contenido.eventos');
});
Route::get('/contactos', function() {
    return view('contenido.contactos');
});
Route::get('/paquetes', function() {
     $paquetes= array(
        ['nombre' => 'p_acojinado', 'titulo' => 'Silla Acojinada con mesa rectangular', 'img' => 'img/paquetes/p_acojinado.png'],
        ['nombre' => 'p_tiffany', 'titulo' => 'Silla Tiffani con mesa redonda', 'img' => 'img/paquetes/p_tiffany.png'],
        ['nombre' => 'p_costos', 'titulo' => 'Mesa Rectangular', 'img' => 'img/paquetes/p_costos.png'],
    );
    return view('contenido.paquetes',['paquetes'=>$paquetes]);
});
Route::get('/cotizacion', function() {
     $cotizacion= array(
        ['nombre' => 'p_costos1', 'titulo' => '', 'img' => 'img/paquetes/p_costos.png']
    );
     $cotizaciones= array(
        ['id'=>1,'producto' => 'MESA RECTANGULAR', 'precio' => 30],
        ['id'=>2,'producto' => 'MESA REDONDA', 'precio' => 30],
        ['id'=>3,'producto' => 'SILLA PLEGABLE ACOJINADA', 'precio' => 18],
        ['id'=>4,'producto' => 'SILLA TIFFANY', 'precio' => 28],
        ['id'=>5,'producto' => 'MANTEL BLANCO RECTANGULAR', 'precio' => 15],
        ['id'=>6,'producto' => 'MANTEL BLANCO REDONDO', 'precio' => 15],
        ['id'=>7,'producto' => 'CUBREMANTEL- COLOR A ELEGIR', 'precio' => 10],
        ['id'=>8,'producto' => 'FUNDA BLANCA PARA SILLA', 'precio' => 10],
        ['id'=>9,'producto' => 'LISTON PARA LAZO O MOÑO', 'precio' => 5],
    );
    return view('contenido.cotizacion',['cotizacion'=>$cotizacion,'cotizaciones'=>$cotizaciones]);
});

/* mail */
Route::post('contactos/send', 'EnviaMailController@enviar');

/*logueo*/
Route::get('aut', 'Administracion\AdministradorController@login')->name('autenticacion')->middleware('guest');
Route::post('aut/login', 'Administracion\AdministradorController@_login')->name('inicia_admin');
Route::post('aut/logout', 'Administracion\AdministradorController@logout')->name('salir');


/*Administracion de los proveedores*/
Route::get('admin', 'Administracion\AdministradorController@proveedores')->name('proveedores')->middleware('auth');
Route::post('admin/addProveedor', 'Administracion\AdministradorController@add');
Route::post('admin/deleteProveedor', 'Administracion\AdministradorController@delete');
Route::post('admin/updateProveedor/{id}', 'Administracion\AdministradorController@update');

/*Administracion de los usuarios*/
Route::get('users', 'Administracion\AdministradorController@users')->name('users')->middleware('auth');
Route::post('users/addUsers/{id?}', 'Administracion\AdministradorController@addUsers');
Route::post('users/deleteUsers', 'Administracion\AdministradorController@deleteUsers');
Route::post('users/updateUsers/{id}/{id2?}', 'Administracion\AdministradorController@updateUsers');