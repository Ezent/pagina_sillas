@extends('plantilla.welcome')
@section('menu')
@endsection
@section('contenido')
<div style="margin-top: 50px;"></div>
<div class="container">
    <div class="row">
        <div class="col-md-6" style="background: #000;">
            <img ondblclick="page('{{url('/')}}')" class="img-fluid" src="{{asset('img/Logo.png')}}" />
        </div>
        <div class="col-md-6"  style="background: #4C4A4A;">
            <h3 class="color-tema"><p align='center'>Login</p></h3>
            <h5 class="c-small text-danger">{{$errors->first('error')}}</h5>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <form id="login" action="{{route('inicia_admin')}}" method="POST">
                        @csrf
                    <h5 class="color-tema"><p align='left'>Email :</p></h5>
                    <input class="form-control-lg" name="email" placeholder="usuario"/>
                    <span class="c-small text-danger">{{$errors->first('email')}}</span>
                    <h5 class="color-tema espacio"><p align='left'>Contraseña:</p></h5>
                    <input type="password" class="form-control-lg" name="password" placeholder="**********"/>
                     <span class="c-small text-danger">{{$errors->first('password')}}</span>
                    <button type="submit"  class=" espacio btn btn-secondary btn-lg" >Login</button>
                    </form>
                </div>
            </div>


        </div>
    </div>
</div>
<script type="text/javascript">
function page(url){
    window.location=url;
}
</script>
@endsection
@section('pie')
@endsection