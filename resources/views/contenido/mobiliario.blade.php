@extends('plantilla.welcome')
<title>Mobiliario</title>
<link rel="shortcut icon"  href="{{asset('img/Logo.png')}}" />
<body id="m2">
    @section('contenido')
    <div style="margin-top: 50px; "></div>
    <div class="container">
        <h3 class="color-tema espacio">Mesas</h3>
        <div class="row">
            @foreach($mobiliario as $item)
            @if($item['nombre']=='Mesas')
            <div class="col-md-6 espacio">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong">
                    <div class="div-img hidden">
                        <img onclick="pasarImg(this,'{{$item['titulo']}}','{{$item['descripcion']}}')" class="img img-responsive  img-t" src="{{$item['img']}}" height="100%" width="100%"/>  
                        <div style="background: #000; width: 60%" class="txt-centrado ">
                            <!--<h5 class="color-tema">{{$item['titulo']}}</h5>-->
                        </div> 
                    </div>
                </a>
            </div>@endif
            @endforeach
        </div>
        <h3 class="color-tema espacio">Sillas</h3>
        <div class="row espacio">
            @foreach($mobiliario as $item)
            @if($item['nombre']=='Sillas')
            <div class="col-md-6 espacio">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong">
                    <div class="div-img hidden">
                        <img onclick="pasarImg(this,'{{$item['titulo']}}','{{$item['descripcion']}}')" class="img img-responsive  img-t" src="{{$item['img']}}" height="100%" width="100%"/>   
                        <div style="background: #000; width: 60%" class="txt-centrado ">
                            <!--<h5 class="color-tema">{{$item['titulo']}}</h5>-->
                        </div> 
                    </div>
                </a>
            </div>@endif
            @endforeach
        </div>
        <h3 class="color-tema espacio">Manteleria</h3>
        <div class="row">
            @foreach($mobiliario as $item)
            @if($item['nombre']=='Manteleria')
            <div class="col-md-4 espacio">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong">
                    <div class="div-img hidden">
                        <img onclick="pasarImg(this,'{{$item['titulo']}}','{{$item['descripcion']}}')" class="img img-responsive  img-t" src="{{$item['img']}}" height="100%" width="100%"/>  
                        <div style="background: #000; width: 60%" class="txt-centrado ">
                            <!--<h5 class="color-tema">{{$item['titulo']}}</h5>-->
                        </div> 
                    </div>
                </a>
            </div>@endif
            @endforeach
        </div>
    </div>
    <!--Ventana modal-->
    <!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
        Launch demo modal
    </button>-->

    <!-- Modal -->
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="background: #454444">
                <div class="modal-header">
                    <h4 class="modal-title color-tema" id="tituloImg">Cerrar</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" >
                    <p align="center">
                        <img  class="img-fluid" id="imgModal" src="#" />
                    </p>
                </div>
                <!--                <div class="modal-footer" >
                                    <p align='justify'>
                                    <h5 class="c-small" id="contenidoImg"></h5>   
                                    </p>  
                                </div>-->
            </div>
        </div>
    </div>
    @endsection
    <!--Fin ventana modal --->
    <script type="text/javascript">
        function pasarImg(image, nombre, descrip) {
        var titulo = document.getElementById('tituloImg');
        var img = document.getElementById('imgModal');
        var descripcion = document.getElementById('contenidoImg');
        img.src = image.src;
        titulo.textContent = nombre;
        descripcion.textContent = descrip;
        }
    </script>
</body>
