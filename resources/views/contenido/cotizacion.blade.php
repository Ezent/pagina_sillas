@extends('plantilla.welcome')
<title>Cotización</title>
<link rel="shortcut icon"  href="{{asset('img/Logo.png')}}" />
<body id="m5">
    @section('contenido')
    <div style="margin-top: 50px; "></div>
    <div class="container">
        <h3 class="color-tema espacio">Cotiza lo que realmente necesitas !!!</h3>
        <div class="row espacio">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <table class="table table-hover table-responsive table-dark" id="cotizar">
                    <thead>
                        <tr>
                            <th scope="col">MOBILIARIO</th>
                            <th scope="col">COSTO POR PIEZA</th>
                            <th scope="col">UNIDADES</th>
                            <th scope="col">=</th>
                            <th scope="col">RESULTADO</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cotizaciones as $item)
                        <tr>
                            <td>{{$item['producto']}}</td>
                            <td>$ {{$item['precio']}}.00 MXN</td>
                            <td><input id="{{$item['id']}}" class="form-control input-sm" name="unidad" /></td>
                            <td>=</td>
                            <td><input class="form-control-sm" type="number" name="res" disabled /></td>
                        </tr>
                        @endforeach
                        <tr id="t" style="display: none" >
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="pull-right" >Total: </td>
                            <td><input class="form-control-sm" type="number" name="total" disabled /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#cotizar input').keyup(function () {
//                var valueKey = String.fromCharCode(event.which);
                var costos = [30, 30, 18, 28, 15, 15, 10, 10, 5];
                var res = document.getElementsByName('res');
                var show = document.getElementById('t');
                var result = document.getElementsByName('total');
                var i = this.id;
                var num = $('#' + i).val();
                var resultado = num * costos[(i - 1)];
                res[(i - 1)].value = resultado;
                if (total() > 0) {
                    show.style.display = "table-row";
                    result[0].value = total();
                } else {
                    show.style.display = "none";
                }
            });
        });
        function total() {
            var cotiza = document.getElementsByName('res');
            var suma = 0;
            for (j = 0; j < cotiza.length; j++) {
                if (cotiza[j].value > 0) {
                    suma += parseInt(cotiza[j].value);
                }
            }
            return suma;
        }
    </script>
    @endsection
</body>