@extends('plantilla.welcome')
<title>Galeria</title>
<link rel="shortcut icon"  href="{{asset('img/Logo.png')}}" />
<body id="m3">
    @section('contenido')
    <div style="margin-top: 60px; "></div>
    <div class="container">
        <h3 class="color-tema espacio">Posibles combinaciones para mesas y sillas</h3>
        <div class="row espacio">
            @foreach($galeria as $item)
            @if($item['nombre']=='Combinacion')
            <div class="col-md-4 espacio">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong">
                    <div class="div-img hidden">
                        <img onclick="pasarImg(this,'{{$item['titulo']}}','{{$item['descripcion']}}')" class="img img-responsive  img-t" src="{{$item['img']}}" height="100%" width="100%"/>   
                    </div>
                </a>
            </div>@endif
            @endforeach
        </div>
        <h3 class="color-tema espacio">Sillas</h3>
        <div class="row">
            @foreach($galeria as $item)
            @if($item['nombre']=='Combinacion_sillas')
            <div class="col-md-4 espacio">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong">
                    <div class="div-img hidden">
                        <img onclick="pasarImg(this,'{{$item['titulo']}}','{{$item['descripcion']}}')" class="img img-responsive  img-t" src="{{$item['img']}}" height="100%" width="100%"/>  
<!--                        <div style="background: #000; width: 60%" class="txt-centrado ">
                            <h5 class="color-tema">{{$item['titulo']}}</h5>
                        </div> -->
                    </div>
                </a>
            </div>@endif
            @endforeach
        </div>

        <h3 class="color-tema espacio">Arreglos y decoraciones</h3>
        <div class="row">
            @foreach($galeria as $item)
            @if($item['nombre']=='arreglos_&_decoraciones')
            <div class="col-md-4 espacio">
                <a href="#" data-toggle="modal" data-target="#exampleModalLong">
                    <div class="div-img hidden">
                        <img onclick="pasarImg(this,'{{$item['titulo']}}','{{$item['descripcion']}}')" class="img img-responsive  img-t" src="{{$item['img']}}" height="100%" width="100%"/>  
<!--                        <div style="background: #000; width: 60%" class="txt-centrado ">
                            <h5 class="color-tema">{{$item['titulo']}}</h5>
                        </div> -->
                    </div>
                </a>
            </div>@endif
            @endforeach
        </div>
    </div>
    <!--Ventana modal-->
    <!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
        Launch demo modal
    </button>-->

    <!-- Modal -->
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div id="unoModal" class="modal-dialog modal-xl" role="document">
            <div class="modal-content" style="background: #454444">
                <div class="modal-header">
                    <h4 class="modal-title color-tema" id="tituloImg">Name Image</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p align="center">
                        <img class="img-fluid img-modal" id="imgModal" src="#" />
                    </p>
                </div>
<!--                <div class="modal-footer" >
                    <h5 class="c-small" >
                    <p align='center' id="contenidoImg"></p>   
                    </h5>  
                </div>-->
            </div>
        </div>
    </div>
    @endsection
    <!--Fin ventana modal --->
    <script type="text/javascript">
        function pasarImg(image, nombre, descrip) {
        var titulo = document.getElementById('tituloImg');
        var img = document.getElementById('imgModal');
        var descripcion = document.getElementById('contenidoImg');
        var sizeModal = document.getElementById('unoModal');
        img.src = image.src;
//        sizeModal.className="modal-dialog modal-lg";
        titulo.textContent = nombre;
        descripcion.textContent = descrip;
        }
    </script>
</body>
