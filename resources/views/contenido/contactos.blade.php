@extends('plantilla.welcome')
<title>Contacto</title>
<link rel="shortcut icon"  href="{{asset('img/Logo.png')}}" />
<body id="m7" >
    @section('contenido')
    <div style="margin-top: 50px; "></div>
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <h3 class="color-tema">Nuestra Ubicación</h3>
            </div>
        </div>
        <div class="row espacio">
            <div class="col-lg">
                <iframe  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d931.130718336046!2d-89.5975216707991!3d21.011754599125478!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f5676c5339189b3%3A0x27c19164ef4eca61!2sCalle+4+Diagonal+150%2C+Residencial+Montecristo%2C+97133+M%C3%A9rida%2C+Yuc.!5e0!3m2!1ses-419!2smx!4v1540531933850"
                         width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="row espacio">
            <div class="col-md-5">
                <h3 class="color-tema" style="text-align: center;">Información de Contacto</h3>
                <hr style="border: 1px solid #8d9499;">
                <p class="c-small"> Calle 4, numero 150 por 11 y 15, Fraccionamiento Montecristo, Merida Yuc.<p>
                <p class="c-small"> Telefono de oficina:</p>
                <p class="c-small"> <span class="fa fa-phone"></span> 999 944 1582</p>
                <p class="c-small"> Telefono de Móvil:</p>
                <p class="c-small"> <span class="fa fa-whatsapp"></span> 999 112 2153</p>
                <div style="height: 80px;"></div>
                <div>
                    <a href="https://www.facebook.com/00montecristo/" class="btn btn-primary btn-lg fa fa-facebook fa-2x"></a>
                    <a href="mailto:montecristo.eventos.mid@gmail.com" class="btn btn-default fa fa-comment fa-2x" style="background: #AADEF7"></a>
                    <a href="https://www.instagram.com" class="btn btn-danger btn-lg fa fa-instagram fa-2x"></a>

                </div>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5">
                <h3 class="color-tema" style="text-align: center;"> Envianos un Mensaje</h3>
                <hr style="border: 1px solid #8d9499;">
                <p class="c-small"> Nos preocupamos por ti y tu opinion nos interesa, cualquier duda, aclaracion y/o cotización,
                    nosotros te la realizamos, mandanos tu mensaje.<p>
                <h5 class="text-white">Correo: </h5>
                <form id="msjForm" onsubmit="enviar(); return false;" action="{{url('/contactos/send')}}"  method="POST">
                    @csrf
                    <label class="text text-white">Nombre: </label>
                    <input id="nombre" type="text" class="form-control" name="nombre" placeholder=" Tu nombre completo..." required/>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="text text-white">Asunto: </label>
                            <input id="asunto" type="text" class="form-control" name="asunto" placeholder="Ej. cotizacion" required/>
                        </div>
                        <div class="col-md-8">
                            <label class="text text-white">Email: </label>
                            <input id="correo" type="email" class="form-control color-tema" name="correo" placeholder="nombre@dominio.com" required/>
                        </div>
                    </div>
                    <div>
                        <label class="text text-white">Mensaje: </label>
                        <textarea id="msj" id="msj" type="text" class="form-control" name="msj" placeholder="Tu comentario..." required ></textarea>
                        <button  type="submit" class="espacio btn btn-success btn-sm" ><span class="fa fa-sign-in"></span> Enviar</button>
 <!--<button type="reset" class="espacio btn btn-warning btn-sm" ><span class="fa fa-close"></span> Descartar</button>-->
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection 
    <script type="text/javascript">
        function enviar() {
            var getForm = $('#msjForm');
            var url = getForm.attr("action")
            var data = getForm.serialize();
            $.post(url, data, function (resultado) {
                $('#nombre').val('');
                $('#asunto').val('');
                $('#correo').val('');
                $('#msj').val('');
                alert("Succes: " + resultado);
            }).fail(function(error,status, errorThrown){
            alert(status+': '+errorThrown);
        });

        }
    </script>
</body>