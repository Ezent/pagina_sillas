@extends('plantilla.welcome')
<body id="m6">
    @section('contenido')
    <div style="margin-top: 100px; "></div>

    <div class="container">
        <div class="row">
            <div class="col-md 6">
                <h2>
                    <p align="center"  class="color-tema"> Nuestros eventos</p>
                </h2>
                <h4><p align="center" class="c-small">
                        A continuacion mostramos una galeria de imagenes de los eventos en los que <b>MONTECRISTO eventos</b>
                        ha estado presente.
                    </p></h4>
            </div>
        </div>
        <div class="row espacio">
            <div class="col-md-12">
                <div id="eventos" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="{{asset('img/eventos/reuniones/reunion2.jpg')}}" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{asset('img/eventos/reuniones/reunion4.jpg')}}" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{asset('img/eventos/quince/quince1.jpg')}}" alt="Third slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{asset('img/eventos/bodas/boda1.jpg')}}" alt="Third slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{asset('img/eventos/bodas/boda2.jpg')}}" alt="Third slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#eventos" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#eventos" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    @endsection
</body>
