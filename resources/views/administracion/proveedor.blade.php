@extends('plantilla.welcome')
@section('menu')
<nav class="navbar navbar-expand-lg fixed-top navbar-dark "  >
    <div class="col-md-1"></div>
    <img ondblclick="page('{{url('/')}}')" class="navbar-brand" src="{{asset('img/icono.png')}}" height="100px" width="13%" alt="logo"/>
    <!-- <a class="navbar-brand text-white " href="#"><h1 style="font-variant: small-caps; color: #ffbf00;">MONTECRISTO</h1>
         <img src="{{asset('img/guion.JPG')}}" height="3px" style="margin-left: 15px; margin-right: 15px;"/>
         <label style="font: oblique  cursive; color: #ffbf00;">eventos</label></a>-->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText" >
        <ul class="navbar-nav ml-auto">
            <li id="uno"  class="nav-item">
                <a class="nav-link" ><h6><span class="fa fa-home"></span> Administrador</h4> <span class="sr-only">(current)</span></h6></a>
            </li>
            <li id="uno"  class="nav-item">
                <form action="{{route('salir')}}" method="POST">
                    @csrf
                    <input type="hidden" value="true" name="sesion"/>
                    <button type="submit" class="nav-link btn btn-danger" ><span class="fa fa-window-close"></span> Cerrar Sesion</button>
                </form>
            </li>
            <!--            <li id="dos" class="nav-item">
                            <a class="nav-link" href="/"><h6><span class="fa fa-universal-access"></span> Proveedores</h6></a>
                        </li>-->
        </ul>
    </div>
</nav> 
@endsection
@section('contenido')
<div style="margin-top: 100px;"></div>
<div class="container">
    <div class="row">
        <div class="pull-left c-small"><h6>
                <span class="fa fa-user-circle"></span> Vienvenido a tu sesión:
                <strong><a href="{{route('users')}}">{{auth()->user()->nombre}}</a></strong></h6></div>
    </div>
</div>
<div class="container ">
    <div class="pull-right"><button class="btn btn-success btn-xs" onclick="agregar('{{url('admin/addProveedor')}}')" data-toggle="modal" data-target="#exampleModal">
            <span class="fa fa-user"></span> Agregar</button></div>
    <h3 class="color-tema espacio"><p>Proeveedores</p></h3>
    <div class="row ">
        <div class="col-md-12">
            @if($proveedor->isEmpty())
            <h1 class="alert alert-dark">No hay proveedores dados de alta</h1>
            @else
            <table class="table table-responsive-lg table-striped  table-dark">
                <thead>
                    <tr style="text-align: center">
                        <th scope="col">Nombre: </th>
                        <th scope="col">Empresa: </th>
                        <th scope="col">Pagina: </th>
                        <th scope="col">Facebook</th>
                        <th scope="col">Tel.</th>
                        <th scope="col">Cel.</th>
                        <th colspan="2">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($proveedor as $item)
                    <tr style="text-align: center">
                        <td>{{$item->nombre}}</td>
                        <td>{{$item->empresa}}</td>
                        <td>{{$item->pagina}}</td>
                        <td>{{$item->fb}}</td>
                        <td>{{$item->telefono}}</td>
                        <td>{{$item->tel_cel}}</td>
                        <td><a class="btn btn-secondary" onclick="editar({{json_encode($item)}},'{{url('admin/updateProveedor')}}')" data-toggle="modal" data-target="#exampleModal">Editar</a></td>
                        <td><a class="btn btn-danger" onclick="eliminar('{{url('admin/deleteProveedor')}}','{{$item->id}}')">Eliminar</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background: #454444">
            <div class="modal-header" >
                <h5 class="modal-title color-tema" id="exampleModalLabel"><strong>Agregar Proveedor</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addP" action="admin/addProveedor" onsubmit="return false" method="POST">
                @csrf
                <input id="flag" type="hidden"  name="bandera" value="agregar"/>
                <div class="modal-body">
                    <i class="text-danger pull-right">* Importante</i><br>
                    <label class="color-tema"><i class="text-danger">*</i> <strong>Nombre del Proveedor :</strong></label>
                    <input class="form-control" name="nombre"  placeholder="Nombre de la persona a cargo"/>
                    <label class="color-tema" ><strong>Empresa :</strong> </label>
                    <input class="form-control" name="empresa"  placeholder="Nombre de la empresa"/>
                    <label class="color-tema"><strong>Link pagina :</strong></label>
                    <input class="form-control" name="pagina" placeholder="url" />
                    <label class="color-tema"><i class="text-danger">*</i> <strong> Link Facebook : </strong></label>
                    <input class="form-control" name="fb" placeholder="url"  />
                    <label class="color-tema"><i class="text-danger">*</i> <strong>Telefono : </strong></label>
                    <input class="form-control" name="tel" placeholder="999 000 000" />
                    <label class="color-tema"><i class="text-danger">*</i> <strong>Celular : </strong></label>
                    <input class="form-control" name="cel" placeholder="999 000 000" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button  onclick="add()" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
<script type="text/javascript">
    function add() {
    if (update() == 0){
    var getForm = $('#addP');
    var url = getForm.attr("action");
    var data = getForm.serialize();
    if (validarCampos() > 0){
    $.post(url, data, function (res) {
    console.log(res);
    window.location = '/admin';
    });
    }
    } else{
    return 0;
    }
    }

    function validarCampos() {
    var nombre = document.getElementsByName('nombre');
    var fb = document.getElementsByName('fb');
    var tel = document.getElementsByName('tel');
    var cel = document.getElementsByName('cel');
    var patron = /[A-Za-z]/;
    var flag = 1;
    if (nombre[0].value == '' || fb[0].value == '' || tel[0].value == '' || cel[0].value == '') {
    alert('Llena todo los campos obligatorios *');
    flag = 0;
    }
    else if (patron.test(tel[0].value) || patron.test(cel[0].value)) {
    alert('Hay campos que no admiten letras');
    flag = 0;
    }
    return flag;
    }

    function editar(dato, url){
    var flag = document.getElementById('flag');
    var form = document.getElementById('addP');
    flag.value = "editar";
    form.action = url + '/' + dato.id;
    var nombre = document.getElementsByName('nombre');
    var empresa = document.getElementsByName('empresa');
    var pagina = document.getElementsByName('pagina');
    var fb = document.getElementsByName('fb');
    var tel = document.getElementsByName('tel');
    var cel = document.getElementsByName('cel');
    $('#exampleModalLabel').html('Editar Proveedor');
    nombre[0].value = dato.nombre;
    empresa[0].value = dato.empresa;
    pagina[0].value = dato.pagina;
    fb[0].value = dato.fb;
    tel[0].value = dato.telefono;
    cel[0].value = dato.tel_cel;
    }
    function update(){
    var flag = document.getElementById('flag');
    if (flag.value == 'editar'){
    var getForm = $('#addP');
    var url = getForm.attr("action");
    var data = getForm.serialize();
    if (validarCampos() > 0){
    $.post(url, data, function (res) {
    console.log(res);
    window.location = '/admin';
    });
    }
    return 1;
    } else{
    return 0;
    }
    }
    function agregar(url){
    var flag = document.getElementById('flag');
    var form = document.getElementById('addP');
    flag.value = "agregar";
    form.action = url;
    $('#exampleModalLabel').html('Agregar Proveedor');
    var entradas = $('#addP input');
    for (i = 0; i < entradas.length; i++){
    if (entradas[i].name == '_token' || entradas[i].name == 'bandera'){
    continue;
    }
    entradas[i].value = '';
    }
    }

    function eliminar(url, id){
    var token = document.getElementsByName('_token');
    var data = '_token=' + token[0].value + '&id=' + id;
    url = url;
    if (confirm("Estas seguro de eliminar este registro")){
    $.post(url, data, function(succes){
    console.log(succes);
    window.location = '/admin';
    }).fail(function(msj){
    alert(msj.responseTxt);
    });
    }
    }
    function page(url){
        window.location=url;
    }
</script>
@section('pie')
@endsection