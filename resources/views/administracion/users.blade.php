@extends('plantilla.welcome')
@section('menu')
<nav class="navbar navbar-expand-lg fixed-top navbar-dark "  >
    <div class="col-md-1"></div>
    <img ondblclick="page('{{url('/')}}')" class="navbar-brand" src="{{asset('img/icono.png')}}" height="100px" width="13%" alt="logo"/>
    <!-- <a class="navbar-brand text-white " href="#"><h1 style="font-variant: small-caps; color: #ffbf00;">MONTECRISTO</h1>
         <img src="{{asset('img/guion.JPG')}}" height="3px" style="margin-left: 15px; margin-right: 15px;"/>
         <label style="font: oblique  cursive; color: #ffbf00;">eventos</label></a>-->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText" >
        <ul class="navbar-nav ml-auto">
            <li id="uno"  class="nav-item">
                <a class="nav-link" href="{{route('proveedores')}}"><h6><span class="fa fa-home"></span> Proveedores</h4> <span class="sr-only">(current)</span></h6></a>
            </li>
            <li id="uno"  class="nav-item">
                <form action="{{route('salir')}}" method="POST">
                    @csrf
                    <input type="hidden" value="true" name="sesion"/>
                    <button type="submit" class="nav-link btn btn-danger" ><span class="fa fa-window-close"></span> Cerrar Sesion</button>
                </form>
            </li>
            <!--            <li id="dos" class="nav-item">
                            <a class="nav-link" href="/"><h6><span class="fa fa-universal-access"></span> Proveedores</h6></a>
                        </li>-->
        </ul>
    </div>
</nav> 
@endsection
@section('contenido')
<div style="margin-top: 100px;"></div>
<div class="container">
    <div class="row">
        <div class="pull-left c-small"><h6><span class="fa fa-user-circle"></span> Vienvenido a tu sesión: <strong>{{auth()->user()->nombre}}</strong></h6></div>
    </div>
</div>
<div class="container ">
    <div class="pull-right"><button class="btn btn-success btn-xs" onclick="agregar('{{url('users/addUsers')}}')" data-toggle="modal" data-target="#exampleModal">
            <span class="fa fa-user"></span> Agregar</button></div>
    <h3 class="color-tema espacio"><p>Usuarios</p></h3>
    <div class="row ">
        <div class="col-md-12">
            <table class="table table-responsive-lg table-striped  table-dark">
                <thead>
                    <tr style="text-align: center">
                        <th scope="col">Nombre: </th>
                        <th scope="col">Email: </th>
                        <th scope="col">Clave: </th>
                        <th scope="col">Alta</th>
                        <th colspan="2">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $item)
                    <tr style="text-align: center">
                        <td>{{$item->nombre}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->clave}}</td>
                        <td>{{$item->registro}}</td>
                        @if($item->email==auth()->user()->email)
                        <td><a class="btn btn-secondary" onclick="editar({{json_encode($item)}},'{{url('users/updateUsers')}}')" data-toggle="modal" data-target="#exampleModal">Editar</a></td>
                        <td><button class="btn btn-danger" onclick="eliminar('{{url('users/deleteUsers')}}','{{$item->id}}')" disabled="">Eliminar</button></td>
                        @else
                        <td><a class="btn btn-secondary" onclick="editar({{json_encode($item)}},'{{url('users/updateUsers')}}')" data-toggle="modal" data-target="#exampleModal">Editar</a></td>
                        <td><a class="btn btn-danger" onclick="eliminar('{{url('users/deleteUsers')}}','{{$item->id}}')">Eliminar</a></td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background: #454444">
            <div class="modal-header">
                <h5 class="modal-title color-tema" id="exampleModalLabel">Agregar Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addS" action="admin/addProveedor" onsubmit="return false" method="POST">
                @csrf
                <input id="flag" type="hidden"  name="bandera" value="agregar"/>
                <div class="modal-body">
                    <i class="text-danger pull-right">* Importante</i><br>
                    <label class="color-tema"><i class="text-danger">*</i> Nombre : </label>
                    <input class="form-control" name="nombre"  placeholder="Nombre del usuario"/>
                    <label class="color-tema"><i class="text-danger">*</i> Email : </label>
                    <input class="form-control" name="email"  placeholder="user@domain.com"/>
                    <label class="color-tema"><i class="text-danger">*</i> Contraseña : </label>
                    <input class="form-control" type="password" name="password" placeholder="*********" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button  onclick="add()" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
<script type="text/javascript">
    function add() {
    if (update() == 0){
    var getForm = $('#addS');
    var url = getForm.attr("action");
    var data = getForm.serialize();
    if (validarCampos() > 0){
    email(data, url+ '/1');
    $.post(url, data, function (res) {
    console.log(res);
    window.location = '/users';
    }).fail(function(er){
    console.log(er.responseTxt);
    });
    }
    } else{
    return 0;
    }
    }
    /*funcion validar email*/
    function email(data, url){
    $.post(url, data, function (res) {
    if (res == 'denegado'){
    alert('Email en uso, elige otro!!!');
    return;
    }
    });
    }

    function validarCampos() {
    var nombre = document.getElementsByName('nombre');
    var email = document.getElementsByName('email');
    var clave = document.getElementsByName('password');
    var flag = 1;
    if (nombre[0].value == '' || email[0].value == '' || clave[0].value == '') {
    alert('Llena todo los campos obligatorios *');
    flag = 0;
    }
    return flag;
    }

    function editar(dato, url){
    var flag = document.getElementById('flag');
    var form = document.getElementById('addS');
    flag.value = "editar";
    form.action = url + '/' + dato.id;
    var nombre = document.getElementsByName('nombre');
    var email = document.getElementsByName('email');
    var clave = document.getElementsByName('password');
    $('#exampleModalLabel').html('Editar Usuario');
    nombre[0].value = dato.nombre;
    email[0].value = dato.email;
    clave[0].value = dato.clave;
    }
    function update(){
    var flag = document.getElementById('flag');
    if (flag.value == 'editar'){
    var getForm = $('#addS');
    var url = getForm.attr("action");
    var data = getForm.serialize();
    if (validarCampos() > 0){
    email(data, url+'/open');
    $.post(url, data, function (res) {
    console.log(res);
    window.location = '/users';
    });
    }
    return 1;
    } else{
    return 0;
    }
    }
    function agregar(url){
    var flag = document.getElementById('flag');
    var form = document.getElementById('addS');
    $('#exampleModalLabel').html('Agregar Usuario');
    flag.value = "agregar";
    form.action = url;
    var entradas = $('#addS input');
    for (i = 0; i < entradas.length; i++){
    if (entradas[i].name == '_token' || entradas[i].name == 'bandera'){
    continue;
    }
    entradas[i].value = '';
    }
    }

    function eliminar(url, id){
    var token = document.getElementsByName('_token');
    var data = '_token=' + token[0].value + '&id=' + id;
    url = url;
    if (confirm("Estas seguro de eliminar este registro")){
    $.post(url, data, function(succes){
    console.log(succes);
    window.location = '/users';
    }).fail(function(msj){
    alert(msj.responseTxt);
    });
    }
    }
    function page(url){
        window.location=url;
    }
</script>
@section('pie')
@endsection