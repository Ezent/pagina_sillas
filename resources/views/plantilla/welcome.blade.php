<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="google-site-verification" content="IolhCsWq3C0NfpL3UiLcDXm_nlImoKRukocU9CoB7bU" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Montecristo eventos confort y calidad en el servicio">
        <meta name="keywords" content="Montecristo eventos, Montecristo eventos Merida, Merida eventos, Alquiladora, Alquiladora de mesas y sillas,
              Alquiladoras Merida, eventos Merida, sillas y mesas Merida">
        <meta name="author" content="Montecristos eventos">
        <title>MONTECRISTO eventos</title>
        <link rel="shortcut icon"  href="{{asset('img/Logo.png')}}" />
        <!-- Fonts -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css"> -->
        <!--Bootstrap 4.1-->
        <link href=" {{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
        <link href=" {{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <!--<link href=" {{ asset('assets/css/carousel.css') }}" rel="stylesheet" type="text/css">-->
        <link href=" {{ asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css">
        <link href=" {{ asset('assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href=" {{ asset('assets/css/estilo.css') }}" rel="stylesheet" type="text/css">
        <script src="{{ asset('assets/js/bootstrap.js') }}"  type="text/javascript"></script>
        <script src="{{ asset('assets/js/jquery.min.js') }}"  type="text/javascript"></script>
        <script src="{{ asset('assets/js/jquery.js') }}"  type="text/javascript"></script>
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"  type="text/javascript"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130747717-2"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments); }
        gtag('js', new Date());
        gtag('config', 'UA-130747717-2');</script>
    </head>
    <body id="m1" style="background: #1D1E20">
        @section('menu')
        <header>
            <nav class="navbar navbar-expand-lg fixed-top navbar-dark "  >
                <div class="col-md-1"></div>
                <img  ondblclick="login('{{route('autenticacion')}}')" class="navbar-brand  " height="100px" width="150px" src="{{asset('img/icono.png')}}" />
                <!-- <a class="navbar-brand text-white " href="#"><h1 style="font-variant: small-caps; color: #ffbf00;">MONTECRISTO</h1>
                     <img src="{{asset('img/guion.JPG')}}" height="3px" style="margin-left: 15px; margin-right: 15px;"/>
                     <label style="font: oblique  cursive; color: #ffbf00;">eventos</label></a>-->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarText" >
                    <ul class="navbar-nav ml-auto">
                        <li id="uno"  class="nav-item">
                            <a class="nav-link" href="/"><h5><span class="fa fa-home"></span> Inicio</h4> <span class="sr-only">(current)</span></h5></a>
                        </li>
                        <li id="dos" class="nav-item">
                            <a class="nav-link" href="/mobiliario"><h5><span class="fa fa-universal-access"></span> Mobiliario</h5></a>
                        </li>
                        <li id="tres" class="nav-item">
                            <a class="nav-link" href="/galeria"><h5><span class="fa fa-camera"></span> Galeria</h5></a>
                        </li>
                        <li id="cuatro" class="nav-item">
                            <a class="nav-link text-white" href="/paquetes"><h5><span class="fa fa-tags"></span> Paquetes</h5></a>
                        </li>
                        <li  id="cinco" class="nav-item">
                            <a class="nav-link" href="/cotizacion"><h5><span class="fa fa-money" ></span> Cotizaciones</h5></a>
                        </li>
                        <!--                        <li id="seis" class="nav-item">
                                                    <a class="nav-link" href="/eventos"><h6><span class="fa fa-users"></span> Eventos</h6></a>
                                                </li>-->
                        <li  id="siete" class="nav-item">
                            <a class="nav-link" href="/contactos"><h5><span class="fa fa-user"></span> Contacto</h5></a>
                        </li>
                    </ul>
                </div>
            </nav> 
        </div>
    </header>
    @show
    <!--Carrusel-->
    @section('contenido')
    <main role="main">
        <div style="height: 10px;"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                            <!--<li data-target="#carouselExampleIndicators" data-slide-to="4"></li>-->
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">

                                <img class="d-block w-100 mh-100 img-responsive ajustarImg"  src="{{asset('img/montecristo_fotos/SAM_2.jpg')}}" >
                                <div class="carousel-caption d-none d-md-block">
                                    <h3 class="color-tema">
                                        Contamos con una gran variedad de mobiliario
                                    </h3>
                                    <!--                                    <h5>
                                                                            <p>
                                                                                Nosotros nos encargamos de equipar tu sala de fiesta para ese momento especial, contamos con una amplia gama de combinaciones para enventos de quinceaños.
                                                                            </p>
                                                                        </h5>-->
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100 img-responsive ajustarImg" src="{{ asset('img/montecristo_fotos/SAM_6.jpg')}}"  >
                                <div class="carousel-caption d-none d-md-block">
                                    <h3 class="color-tema">
                                        Decoraciones para la ocasión
                                    </h3>
                                    <!--                                    <h5>
                                                                            <p>
                                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                                            </p>
                                                                        </h5>-->
                                </div>
                            </div>

                            <div class="carousel-item">
                                <img class="d-block w-100 img-responsive ajustarImg" src="{{asset('img/montecristo_fotos/SAM_3.jpg')}}">
                                <div class="carousel-caption d-none d-md-block">
                                    <h3 class="color-tema">
                                        Decoraciones Multicolores
                                    </h3>
                                    <!--                                    <h5>
                                                                            <p>
                                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                                            </p>
                                                                        </h5>-->
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100 img-responsive ajustarImg" src="{{asset('img/montecristo_fotos/SAM_11.jpg')}}">
                                <div class="carousel-caption d-none d-md-block">
                                    <h3 class="color-tema">
                                        Decoraciones Especiales
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>    
                </div>
            </div>
        </div>
        <div class="container espacio">
            <div class="row" style="margin: 30px;">
                <div class="col-md-12" style=" border-radius: 3px; height: 60px auto;">
                    <!--<h3 class="color-tema"><p align='center'>MONTECRISTO eventos</p></h3>-->
                    <p align="center"><img  src="{{asset('img/icono.png')}}" alt="logo"/></p>
                    <hr style="border: 1px solid #8d9499;">
                    <h5 class="text-white"> <p align="center">  Empresa dedicada a la renta de mobiliario de gran calidad y elegancia que haran de su evento un exito en el confort de sus invitados </p></h5>
                </div>
            </div>
        </div>

        <div class="container" style="margin-top: 50px;">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <h3 class="color-tema"><p align='center'>Sillas, Mesas, y Manteles</p></h3>
                    <hr style="border: 1px solid #8d9499;">
                </div>
            </div>
            <div class="row espacio">
                <div class="col-md-4">
                    <div class="card" >
                        <a onclick="activarItem(0)" href="" data-toggle="modal" data-target="#exampleModal"><img class="card-img-top" src="{{asset('img/sillas/s4.jpg')}}" alt="Card image cap"></a>
                        <div class="card-body cuerpo-oscuro-transparente">
                            <h5 class="card-title color-tema">Juego de sillas tiffany con mantel blanco</h5>
                            <p class="card-text text-white">
                                El juego de sillas tiffany sugiere una combinación basica pero elegante, con sillas tiffany y cojin blanco, mesa redonda y mantel blanco. Asi como esta combinacion, puedes sugerir otra.
                            </p>
                            <a  href="{{url('/galeria')}}" class=" btn btn-secondary">Sugerencias</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" >
                        <a onclick="activarItem(1)" href=""  data-toggle="modal" data-target="#exampleModal"><img class="card-img-top" src="{{asset('img/sillas/s5.jpg')}}" alt="Card image cap"></a>
                        <div class="card-body cuerpo-oscuro-transparente">
                            <h5 class="card-title color-tema">Juego de sillas tiffany con mantel de color</h5>
                            <p class="card-text text-white">
                                El juego esta compuesto por sillas tiffany con cojin blanco, mesa redonda con mantel para mesa redonda color azul pastel. Asi como esta combinacion, puedes sugerir otra.
                            </p>
                            <a href="{{url('/galeria')}}" class="btn btn-secondary">Sugerencias</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" >
                        <a onclick="activarItem(2)" href=""  data-toggle="modal" data-target="#exampleModal"><img class="card-img-top" src="{{asset('img/sillas/s9.jpg')}}" alt="Card image cap"></a>
                        <div class="card-body cuerpo-oscuro-transparente">
                            <h5 class="card-title color-tema">Juego de sillas plegables y decoracion de colores</h5>
                            <p class="card-text text-white">
                                Este juego incluye sillas plegables acojinadas con funda y lazo color azul, mesa tipo redonda con mantel blanco y cubremantel azul,
                                asi como esta combinacion, puedes sugerir otra.
                            </p>
                            <a  href="{{url('/galeria')}}" class="btn btn-secondary">Sugerencias</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="margin-top: 50px">
            <div class="row">
                <div class="col-md-4">
                    <h3 class="color-tema"><p align='center'>Eventos</p></h3>
                    <hr style="border: 1px solid #8d9499;">
                    <h6 class="espacio text-white">
                        Dejanos sorprenderte, si tienes planeado realizar un evento para ese dia especial, MONTECRISTO eventos te ofrece
                        las mejores propuestas para hacer de ese dia, un día inolvidable, contamos con un amplio mobiliario de mesas, sillas y manteles,
                        te asesoramos en la decoración de tu evento y llevamos todo lo que necesites hasta la puerta de tu casa, también podemos 
                        realizarte cotizaciones o si deseas puedes cotizar tus precios en la seccion de cotización, cualquier duda o aclaracion, no olvides ponerte en contacto con nosotros.
                    </h6>
                </div>
                <div class="col-md-8">
                    <div id="eventos" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100 ajustarImg2" src="{{asset('img/montecristo_fotos/SAM_1.jpg')}}" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100 ajustarImg2" src="{{asset('img/montecristo_fotos/SAM_8.jpg')}}" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100 ajustarImg2" src="{{asset('img/montecristo_fotos/SAM_9.jpg')}}" alt="Third slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100 ajustarImg2" src="{{asset('img/montecristo_fotos/SAM_10.jpg')}}" alt="Third slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100 ajustarImg2" src="{{asset('img/montecristo_fotos/SAM_12.jpg')}}" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#eventos" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#eventos" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @show
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document" style="margin-top: 70px;">
                <div class="modal-content  modal-backdrop">
                    <div class="modal-header" style="border: none">
                        <!--<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>-->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span class="fa fa-close" aria-hidden="true"></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!--Aqui vamos a meterle todo nuestro carusel -->
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div name="m_c" class="carousel-item">
                                    <img class="d-block w-100" src="{{asset('img/sillas/s4.jpg')}}" alt="First slide">
                                </div>
                                <div name="m_c" class="carousel-item">
                                    <img class="d-block w-100" src="{{asset('img/sillas/s5.jpg')}}" alt="Second slide">
                                </div>
                                <div name="m_c" class="carousel-item">
                                    <img class="d-block w-100" src="{{asset('img/sillas/s9.jpg')}}" alt="Third slide">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <!--                    <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                        </div>-->
                </div>
            </div>
        </div>

        @section('pie')
        <footer class="color-pie ">
            <div class="container">
                <div class="row" style="margin-top: 100px;">
                    <div class="col-md-4 espacio " style=" text-align: center;">
                        <h6 class="text-white">CONTACTO:</h6>
                        <p class="c-small">Calle 4, numero 150 por 11 y 15, Fraccionamiento Montecristo,Merida Yuc</p>
                        <p class="c-small"><span class="fa fa-phone"></span> 999 944 1582 - <span class="fa fa-whatsapp"></span> 999 112 2153</p>
                    </div>
                    <div        class="col-md-4 espacio" style=" text-align: center;">
                        <h6 class="text-white">Nuestras redes sociales: </h6>
                        <a class="espacio btn btn-secondary" href="https://www.facebook.com/00montecristo/"><span class="fa fa-facebook"></span></a>
                        <a class="espacio btn btn-secondary" href="mailto:montecristo.eventos.mid@gmail.com"><span class="fa fa-comment"></span></a>
                        <a class="espacio btn btn-secondary" href="https://www.instagram.com"><span class="fa fa-instagram"></span></a>
                    </div>
                    <div class="col-md-4" style="text-align: center;">
                        <p style="text-align: center;">
                            <img src="{{asset('img/icono.png')}}" height="100px" width="150px" style="margin-top: 50px;" />
                        </p>
                    </div>
                </div>
            </div>
        </footer>
        @show
        <a class="ir-arriba"  href="#" title="Volver arriba">
            <span class="fa-stack">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-arrow-up fa-stack-1x fa-inverse"></i>
            </span>
        </a>
        <!--javascript para efectos -->
        <script type="text/javascript">
            $(document).ready(function () {
            $('.ir-arriba').click(function () {
            $('body, html').animate({
            scrollTop: '0px'
            }, 1000);
            });
            $(window).scroll(function () {
            if ($(this).scrollTop() > 0) {
            $('.ir-arriba').slideDown(600);
            } else {
            $('.ir-arriba').slideUp(600);
            }
            }); /*hacia abajo*/
            $('.ir-abajo').click(function () {
            $('body, html').animate({
            scrollTop: '1000px'
            }, 1000);
            });
            });
            function login(url){
            window.location = url;
            }

            function activarItem(activar){
            var item = document.getElementsByName('m_c');
            for (var i = 0; i < item.length; i++) {
            if (i == activar){
            item[i].className = "carousel-item active";
            } else{
            item[i].className = "carousel-item";
            }
            }
            }
        </script>
        <!-- termina efectos js-->
</body>
</html>