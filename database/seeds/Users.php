<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'nombre'=>'Antonio Mondragon',
            'email'=>'montecristo.eventos.mid@gmail.com',
            'password'=> bcrypt('2018montecristo'),
            'clave'=>'2018montecristo'
        ]);
    }
}
