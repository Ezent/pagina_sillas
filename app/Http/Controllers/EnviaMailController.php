<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Session;

class EnviaMailController extends Controller {

    public $subject;
    public $from;

    public function enviar(Request $request) {
        if($request->ajax()){
            $this->subject = $request->asunto;
            $this->from = $request->asunto;
            Mail::send('emails.msj', $request->all(), function($msj) {

                $msj->subject($this->subject);
                $msj->to('montecristo.eventos.mid@gmail.com');
            });
        }

        return 'Mensaje enviado correctamente';
    }

}
