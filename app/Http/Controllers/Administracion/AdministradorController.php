<?php

namespace App\Http\Controllers\Administracion;

use App\Proveedor;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdministradorController extends Controller {
    /* Contructor para validar quien pasa a la sesion */

    function __construct() {
//        $this->middleware('auth',['only'=>'proveedores']);
    }

    public function proveedores() {
//        $proveedores= Proveedor::all();
        $proveedores = \Illuminate\Support\Facades\DB::table('proveedores')->get();
        return view('administracion.proveedor', ["proveedor" => $proveedores]);
    }

    public function add(Request $request) {
        if ($request->ajax()) {
            $add_p = new Proveedor();
            $add_p->nombre = $request->nombre;
            $add_p->empresa = $request->empresa;
            $add_p->pagina = $request->pagina;
            $add_p->fb = $request->fb;
            $add_p->telefono = $request->tel;
            $add_p->tel_cel = $request->cel;
            $add_p->save();
            return 'insertado';
        }
    }

    public function update(Request $request, $id) {
        if ($request->ajax()) {
            $proveedor = Proveedor::find($id);
            $proveedor->nombre = $request->nombre;
            $proveedor->empresa = $request->empresa;
            $proveedor->pagina = $request->pagina;
            $proveedor->fb = $request->fb;
            $proveedor->telefono = $request->tel;
            $proveedor->tel_cel = $request->cel;
            $proveedor->save();
            return "Editado exitosamente";
        }
    }

    public function delete(Request $request) {
        if ($request->ajax()) {
            Proveedor::destroy($request->id);
            return "Eliminado correctamente";
        }
    }

    /* Login & Logout */

    public function login() {
        return view('login.login');
    }

    public function _login(Request $request) {
        $mensajes = ['nombre.required' => 'Elcorreo es requerido', 'pass.required' => 'La contraseña es requerida'];
        $val = $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
                ], $mensajes);
        if (Auth::attempt($val)) {
            return redirect()->route('proveedores');
        } else {
            return back()->withErrors(['error' => 'Estas credenciales no son correctas!!!']);
        }
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect()->route('autenticacion');
    }

    /* Admministrar contraseñas de usuario */

    public function users() {
        $users = User::all();
        return view('administracion.users', ["users" => $users]);
    }

    public function addUsers(Request $request, $id = 0) {
        if ($request->ajax()) {
            if ($request->id == 1) {
                $val = User::where('email', '=', $request->email)->get();
                if ($val->isEmpty()) {
                    return response()->json(['res' => 'aceptado']);
                } else {
                    return "denegado";
                }
            } else {
                $add_usr = new User();
                $add_usr->nombre = $request->nombre;
                $add_usr->email = $request->email;
                $add_usr->password = bcrypt($request->password);
                $add_usr->clave = $request->password;
                $add_usr->save();
                return 'insertado';
            }
        }
    }

    public function validaEmail(Request $request) {
        if ($request->ajax()) {
            $val = User::where('email', '=', $request->email)->get();
            if ($val->isEmpty()) {
                return "aceptado";
            } else {
                return "denegado";
            }
        }
    }

    public function updateUsers(Request $request, $id,$id2="close") {
        if ($request->ajax()) {
            if ($request->id2 == 'open') {
                $val = User::where('email', '=', $request->email)->get();
                if ($val->isEmpty()) {
                    return response()->json(['res' => 'aceptado']);
                } else {
                    return "denegado";
                }
            } else {
                $usr = User::find($id);
                $usr->nombre = $request->nombre;
                $usr->email = $request->email;
                $usr->password = bcrypt($request->password);
                $usr->clave = $request->password;
                $usr->save();
                return "Editado exitosamente";
            }
        }
    }

    public function deleteUsers(Request $request) {
        if ($request->ajax()) {
            User::destroy($request->id);
            return "Eliminado correctamente";
        }
    }

}
