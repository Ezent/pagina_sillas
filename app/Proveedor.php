<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected  $table="proveedores";
    protected $fillable = [
        'nombre', 'empresa', 'pagina','telefono','te_cel'
    ];
    public $timestamps=false;
}
